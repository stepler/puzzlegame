// Core modules
var path = require('path');

// Gulp modules
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    gulpif = require('gulp-if'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    tmp = require('tmp'),
    shell = require('gulp-shell'),
    merge = require('merge-stream'),
    runSequence = require('run-sequence');

// Variables
var ROOT_DIR = path.join(__dirname, '..'),
    STATIC_DIR = path.join(ROOT_DIR, 'static'),
    CSS_DIR = STATIC_DIR+'/css',
    BEM_DIR = STATIC_DIR+'/bem-blocks',
    BEM_RELDIR = BEM_DIR.replace(/\.\.?\//, '/');

var BD_LOGIN = 'simon.moiseenko@gmail.com',
    BD_APP_ID = 1503494;

var WS_ENABLE = false;

/**
 * Работаем с стилями
 */
gulp.task('style-less', function() {
  return gulp.src(BEM_DIR+'/bem.less')
    .pipe(less())
    .pipe(rename({basename: 'style'}))
    .pipe(gulp.dest(CSS_DIR))
    .on('end', function(){
      WS.send({type: 'css', source: '/static/css/style.css'});
    });
});

gulp.task('style-watch', function() {
  gulp.watch(BEM_DIR+'/**/*.less', ['style-less']);
  WS.init();
});


gulp.task('make-screen', shell.task(
  [
    // Android
    'phantomjs rasterize.js <%=URL%> <%=IMG_DIR%>/ios/335x500_1.png 335px*500px',
    'phantomjs rasterize.js <%=URL%>#/level/x3 <%=IMG_DIR%>/ios/335x500_2.png 335px*500px',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/335x500_3.png 335px*500px 3000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/335x500_4.png 335px*500px 10000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%>/complete <%=IMG_DIR%>/ios/335x500_5.png 335px*500px',
    // iPhone 4
    'phantomjs rasterize.js <%=URL%> <%=IMG_DIR%>/ios/640x960_1.png 640px*960px',
    'phantomjs rasterize.js <%=URL%>#/level/x3 <%=IMG_DIR%>/ios/640x960_2.png 640px*960px',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/640x960_3.png 640px*960px 3000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/640x960_4.png 640px*960px 10000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%>/complete <%=IMG_DIR%>/ios/640x960_5.png 640px*960px',
    // iPhone 5
    'phantomjs rasterize.js <%=URL%> <%=IMG_DIR%>/ios/640x1136_1.png 640px*1136px',
    'phantomjs rasterize.js <%=URL%>#/level/x3 <%=IMG_DIR%>/ios/640x1136_2.png 640px*1136px',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/640x1136_3.png 640px*1136px 3000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/640x1136_4.png 640px*1136px 10000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%>/complete <%=IMG_DIR%>/ios/640x1136_5.png 640px*1136px',
    // iPhone 6
    'phantomjs rasterize.js <%=URL%> <%=IMG_DIR%>/ios/750x1334_1.png 750px*1334px',
    'phantomjs rasterize.js <%=URL%>#/level/x4 <%=IMG_DIR%>/ios/750x1334_2.png 750px*1334px',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/750x1334_3.png 750px*1334px 3000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/750x1334_4.png 750px*1334px 10000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%>/complete <%=IMG_DIR%>/ios/750x1334_5.png 750px*1334px',
    // iPhone 6 Plus
    'phantomjs rasterize.js <%=URL%> <%=IMG_DIR%>/ios/1242x2208_1.png 1242px*2208px',
    'phantomjs rasterize.js <%=URL%>#/level/x3 <%=IMG_DIR%>/ios/1242x2208_2.png 1242px*2208px',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/1242x2208_3.png 1242px*2208px 3000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%> <%=IMG_DIR%>/ios/1242x2208_4.png 1242px*2208px 10000',
    'phantomjs rasterize.js <%=URL%>#/game/<%=GAME_ID2%>/complete <%=IMG_DIR%>/ios/1242x2208_5.png 1242px*2208px',
  ], {
    templateData: {
      URL: 'http://127.0.0.1/?make_test',
      IMG_DIR: path.join(ROOT_DIR, 'res', 'promo'),
      GAME_ID1: 'Gx3-2',
      GAME_ID2: 'Gx4-1'
    }
  }
));


/**
 * Собираем приложение через phonegap
 */

gulp.task('build-app', shell.task(
  [
    'cd <%=ROOT_DIR%> && tar -czf <%=TMP_FILE%> index.html config.xml app static libs res/icon res/splash',
    'curl -u <%=BD_LOGIN%> -X PUT -F file=@<%=TMP_FILE%> https://build.phonegap.com/api/v1/apps/<%=BD_APP_ID%>',
    'mv <%=TMP_FILE%> <%=ROOT_DIR%>'
  ], {
    templateData: {
      ROOT_DIR: ROOT_DIR,
      TMP_FILE: tmp.tmpNameSync({ postfix:'.tar.gz' }),
      BD_LOGIN: BD_LOGIN,
      BD_APP_ID: BD_APP_ID
    }
  }
));


/**
 * Собираем проект
 */
gulp.task('build-style', function(cb) {
  runSequence('style-less');
});

gulp.task('build', function() {
  runSequence('build-style')
});


/**
 * WS сервер для WebpageLiveUpdate chrome-плагина
 * INFO: http://git.webpp.ru/tools/project-stub/wikis/WebpageLiveUpdate
 */
var WS = {
  server: null,
  pool: {},
  init: function()
  {
    if (!WS_ENABLE) {
      return;
    }

    var self = this;
    this.server = new WebSocketServer.Server({host:'0.0.0.0', port: 35729});
    this.server.on('connection', function(ws)
    {
      var id = Math.random();
      self.pool[id] = ws;
      // console.log('New connection ' + id);

      ws.on('close', function() {
        // console.log('Connection close ' + id);
        delete self.pool[id];
      });
    });

    // Хак, для поддержки соединения.
    // TODO: сделать нормальное решение.
    setInterval(function(){
      self.send({type: 'ping'});
    }, 5000);
  },
  send: function(data)
  {
    if (!WS_ENABLE) {
      return;
    }

    var message = JSON.stringify(data);
    for(var id in this.pool) {
      this.pool[id].send(message);
    }
  }
};
