/*global require*/
'use strict';

// Require.js allows us to configure shortcut alias
require.config({
  map: {
    '*': {
      'underscore': 'lodash',
      'jquery': 'zepto'
    }
  },
  shim: {
    'zepto': {
      exports: '$'
    },
    lodash: {
      exports: '_'
    },
    backbone: {
      deps: [
        'lodash',
        'zepto'
      ],
      exports: 'Backbone'
    },
    backboneLocalstorage: {
      deps: ['backbone'],
      exports: 'Store'
    }
  },
  paths: {
    fastclick: '../libs/fastclick',
    zepto: '../libs/zepto.min',
    lodash: '../libs/lodash.min',
    backbone: '../libs/backbone/backbone',
    backboneLocalstorage: '../libs/backbone/localstorage',
    text: '../libs/requirejs/text'
  }
});

require([
  'fastclick',
  'backbone',
  'core/bootstrap',
  'core/router',
  'common'
], function (FastClick, Backbone, Bootstrap, Router, Common) {

  FastClick.attach(document.body);

  Bootstrap(function(){
    var router = new Router();
    Backbone.history.start();
    Common.router = router;
  });
});
