/*global define*/
define([
  'zepto',
  'lodash',
  'core/startup',
  'core/ad_mob',
  'collections/settings',
  'collections/levels',
  'collections/games',
  'views/main',
  'views/level',
  'views/game',
  'views/game_complete',
  'views/game_pause',
  'views/error500',
  'common'
], function ($, _, Startup, AdMob, Settings, LevelsCollection, GamesCollection, MV, LV, GV, GCV, GPV, EV, Common) {
  'use strict';

  function init_startup()
  {
    var level;

    window.img_35_8 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAICAYAAABzskasAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAB9JREFUeNpi/P//P8NgAUwMgwiMOmbUMaOOoRYACDAATTkDDTQgP3sAAAAASUVORK5CYII=';
    window.img_16_9 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAYAAAA7KqwyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABpJREFUeNpi/P//PwMlgImBQjBqwLAwACDAAOVfAw9/ZDvcAAAAAElFTkSuQmCC';
    window.img_1_1  = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABpJREFUeNpi/P//PwMxgImBSDCqkDoKAQIMANUSAxFqvpFcAAAAAElFTkSuQmCC';

    Settings.fetch({reset:true});
    Common.isTutorialPass = !!Settings.getKey('isTutorialPass');

    level = Settings.getKey('level');
    $('body').attr('class', 'b-screen '+level);
    if (level) {
      return;
    }

    Startup();
    Settings.setKey('level', 'x3');
    level = Settings.getKey('level');
    $('body').attr('class', 'b-screen '+level);
  }

  function init_collections()
  {
    LevelsCollection.fetch({reset:true});
    GamesCollection.fetch({reset:true});
  }

  function init_views()
  {
    $('#loading').hide();
    if (window.AdMob) {
      $('html').attr('class', 'with-ad');
    }
  }

  function init_ad()
  {
    AdMob();
  }

  return function(callback) {
    init_startup();
    init_collections();
    init_views();
    init_ad();
    callback();
  };
});
