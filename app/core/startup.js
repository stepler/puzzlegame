/*global define*/
define([
  'zepto',
  'lodash',
  'collections/levels',
  'collections/games',
  'collections/settings',
  'common'
], function ($, _, Levels, Games, Settings, Common) {
  'use strict';

  clearToTest();

  return function()
  {
    Settings.setKey('version', '1.0');

    // First Launch
    Levels.create({
      type: 'x3',
      title: '3 &times; 3',
      next_levels: ['x4'],
      is_open: true,
      is_complete: false
    });
    Levels.create({
      type: 'x4',
      title: '4 &times; 4',
      next_levels: ['x5'],
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'x5',
      title: '5 &times; 5',
      next_levels: ['x6'],
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'x6',
      title: '6 &times; 6',
      // next_levels: ['x8'],
      is_open: false,
      is_complete: false
    });
    // Levels.create({
    //   type: 'x7',
    //   title: '7 &times; 7',
    //   next_levels: ['x8'],
    //   is_open: false,
    //   is_complete: false
    // });
    // Levels.create({
    //   type: 'x8',
    //   title: '8 &times; 8',
    //   is_open: false,
    //   is_complete: false
    // });

    var levels = ['x3','x4','x5','x6'],
        def_timer = {'x3': 2, 'x4': 10, 'x5': 30, 'x6': 60};

    _.forEach(levels, function(level, L) {
      var i, open, size, timer, next_game, tmp,
          next_end_game = ['all_complete', 0],
          max_i = level==='x3' ? 6 : 9;
      size = parseInt(level.replace('x', ''));
      for (i=1; i<=max_i; i++)
      {
        open = level==='x3' && i < 2;
        timer = def_timer[level]*1000;

        if (i == max_i) {
          next_game = (levels[L+1] ? [levels[L+1], '1'] : next_end_game)
        }
        else {
          next_game = [level, (i+1).toString()]
        }

        // tmp = size;
        // if (i === 2)
        //   tmp = 4;
        // if (i === 3)
        //   tmp = 6;

        Games.create({
          id: 'G'+level+'-'+i.toString(),
          level: level,
          sort: i*10,
          title: i.toString(),
          alias: i.toString(),
          size: size,
          puzzle: getPuzzle(level, i),
          next_game: next_game,
          is_open: open,
          timer: timer,
          awards_timer: timer
        });
      }

      function F(value)
      {
        return parseInt(value);
      }
    });

    updateToTest();

    function getPuzzle(level, game) 
    {
      if (level === 'x3') 
      {
        if (game === 1)
          return [0,0,0,
                  0,1,0,
                  0,0,0];
        if (game === 2)
          return [1,0,0,
                  0,1,0,
                  0,0,1];
        if (game === 3)
          return [0,1,0,
                  1,0,1,
                  0,1,0];
        if (game === 4)
          return [0,1,0,
                  1,1,1,
                  0,1,0];
        if (game === 5)
          return [0,1,0,
                  1,0,1,
                  1,1,1];
        if (game === 6)
          return [1,0,1,
                  0,1,0,
                  1,0,1];
      }

      if (level === 'x4') 
      {
        if (game === 1)
          return [1,1,1,1,
                  1,0,0,1,
                  1,0,0,1,
                  1,1,1,1];
        if (game === 2)
          return [0,0,0,0,
                  0,0,0,0,
                  1,1,1,1,
                  1,1,1,1];
        if (game === 3)
          return [1,0,0,1,
                  0,1,1,0,
                  0,1,1,0,
                  1,0,0,1];
        if (game === 4)
          return [1,1,0,0,
                  0,1,1,0,
                  0,1,1,0,
                  0,0,1,1];
        if (game === 5)
          return [1,0,0,0,
                  0,1,0,1,
                  0,0,1,1,
                  0,1,1,1];
        if (game === 6)
          return [1,1,1,0,
                  1,1,0,1,
                  1,0,1,1,
                  0,1,1,1];
        if (game === 7)
          return [0,1,1,0,
                  0,1,1,0,
                  1,1,1,1,
                  1,0,0,1];
        if (game === 8)
          return [0,0,1,1,
                  0,0,1,1,
                  1,1,0,0,
                  1,1,0,0];
        if (game === 9)
          return [0,1,1,0,
                  1,0,0,1,
                  1,0,0,1,
                  0,1,1,0];
        if (game === 10)
          return [0,1,1,0,
                  0,1,1,0,
                  1,0,0,1,
                  1,1,1,1];
        if (game === 11)
          return [0,1,1,0,
                  1,1,1,1,
                  0,0,0,0,
                  0,1,1,0];
        if (game === 12)
          return [1,1,1,1,
                  1,0,1,0,
                  1,0,1,0,
                  1,1,1,1];
      }

      if (level === 'x5') {
        if (game === 1)
          return [1,1,1,1,1,
                  1,0,0,0,1,
                  1,0,1,0,1,
                  1,0,0,0,1,
                  1,1,1,1,1];
        if (game === 2)
          return [1,0,0,0,1,
                  0,1,0,1,0,
                  0,0,1,0,0,
                  0,1,0,1,0,
                  1,0,0,0,1];
        if (game === 3)
          return [0,0,1,0,0,
                  0,1,0,1,0,
                  1,0,1,0,1,
                  0,1,0,1,0,
                  0,0,1,0,0];
        if (game === 4)
          return [0,0,1,0,0,
                  0,1,0,1,0,
                  1,1,1,1,1,
                  0,1,0,1,0,
                  0,1,1,1,0];
        if (game === 5)
          return [0,0,1,0,0,
                  0,0,1,0,0,
                  0,1,0,1,0,
                  0,1,0,1,0,
                  1,1,1,1,1];
        if (game === 6)
          return [1,1,0,1,1,
                  1,1,0,1,1,
                  0,0,0,0,0,
                  1,1,0,1,1,
                  1,1,0,1,1];
        if (game === 7)
          return [1,0,1,0,1,
                  0,1,1,1,0,
                  1,1,0,1,1,
                  0,1,1,1,0,
                  1,0,1,0,1];
        if (game === 8)
          return [0,0,1,0,0,
                  0,1,1,1,0,
                  1,0,1,0,1,
                  0,0,1,0,0,
                  1,0,1,0,1];
        if (game === 8)
          return [1,1,1,1,1,
                  0,0,0,0,1,
                  1,1,1,1,1,
                  1,0,0,0,0,
                  1,1,1,1,1];
        if (game === 10)
          return [1,0,1,0,1,
                  0,1,0,1,0,
                  1,0,1,0,1,
                  0,1,0,1,0,
                  1,0,1,0,1];
        if (game === 11)
          return [0,1,1,1,0,
                  1,0,0,0,1,
                  0,1,1,1,0,
                  1,0,0,0,1,
                  0,1,1,1,0];
        if (game === 12)
          return [0,1,0,1,0,
                  1,0,1,0,1,
                  0,1,0,1,0,
                  0,1,0,1,0,
                  1,0,1,0,1];
      }

      if (level === 'x6') {
        if (game === 1)
          return [1,1,1,1,1,1,
                  1,0,0,0,0,1,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1,
                  1,0,0,0,0,1,
                  1,1,1,1,1,1];
        if (game === 2)
          return [0,1,0,0,1,0,
                  0,1,0,0,1,0,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1,
                  0,1,0,0,1,0,
                  0,1,0,0,1,0];
        if (game === 3)
          return [0,0,1,1,0,0,
                  0,1,0,0,1,0,
                  1,1,1,1,1,1,
                  0,1,0,0,1,0,
                  0,1,0,0,1,0,
                  0,1,1,1,1,0];
        if (game === 4)
          return [1,0,1,0,1,0,
                  1,0,0,0,1,0,
                  0,1,0,1,0,1,
                  0,1,0,1,0,1,
                  0,0,1,0,0,0,
                  0,0,1,0,1,0];
        if (game === 5)
          return [0,0,1,1,0,0,
                  0,1,1,1,1,0,
                  1,0,1,1,0,1,
                  1,0,0,0,0,1,
                  0,1,0,0,1,0,
                  0,0,1,1,0,0];
        if (game === 6)
          return [1,1,0,0,1,1,
                  1,1,0,0,1,1,
                  0,0,1,1,0,0,
                  0,0,1,1,0,0,
                  1,1,0,0,1,1,
                  1,1,0,0,1,1];
        if (game === 7)
          return [0,1,0,0,1,0,
                  1,1,1,1,1,0,
                  1,0,0,0,0,1,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1,
                  1,1,1,1,1,1];
        if (game === 8)
          return [0,1,0,0,1,0,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1,
                  0,1,0,0,1,0,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1];
        if (game === 9)
          return [0,1,1,1,1,0,
                  1,0,0,0,0,1,
                  1,0,0,0,0,1,
                  0,1,1,1,1,0,
                  0,1,0,0,1,0,
                  0,1,0,0,1,0];
        if (game === 10)
          return [1,0,1,1,0,1,
                  0,1,0,0,1,0,
                  1,0,1,1,0,1,
                  0,0,1,1,0,0,
                  0,1,0,0,1,0,
                  1,1,0,0,1,1];
        if (game === 11)
          return [1,0,0,0,0,1,
                  0,1,1,1,1,0,
                  0,1,0,0,1,0,
                  0,1,0,0,1,0,
                  1,0,1,1,0,1,
                  1,1,0,0,1,1];
        if (game === 12)
          return [1,1,0,0,1,1,
                  1,0,1,1,0,1,
                  1,0,1,1,0,1,
                  0,0,1,1,0,0,
                  1,0,1,1,0,1,
                  1,1,1,1,1,1];
      }
    }
  };

  function clearToTest()
  {
    if (window.location.search === "?make_test") {
      localStorage.clear();
    }
  }

  function updateToTest()
  {
    if (window.location.search === "?make_test") 
    {
      Levels.findWhere({type: 'x3'}).completeLevel();
      Levels.findWhere({type: 'x4'}).openLevel();
      _.map(Games.where({level: 'x4'}), 
        function(model){ model.openGame();  model.completeGame(); model.setAward(['g','s','b'][_.random(0, 2)]); });
    }
  }
});
