/*global define*/
define([
  'zepto',
  'lodash',
  'common'
], function ($, _, Common) {
  'use strict';

  var scroll_nodes = {x: null, y: null},
      scroll_node_style,
      cell_size = 0,
      move_cb = function(){},
      move_end_cb = function(){};

  var move = false,
      move_axis = false,
      move_direction = 1,
      start_x,
      start_y,
      last_x,
      last_y,
      move_offset;

  var _transform,
      _vendor = (function () {
        var _elementStyle = document.createElement('div').style,
            vendors = ['t', 'webkitT', 'MozT', 'msT', 'OT'],
            transform, i, l;
        for (i=0, l=vendors.length; i<l; i++) {
          transform = vendors[i] + 'ransform';
          if ( transform in _elementStyle ) return vendors[i].substr(0, vendors[i].length-1);
        }
        return false;
      })();
  _transform = _prefixStyle('transform')

  function _prefixStyle (style) {
    if ( _vendor === false ) return false;
    if ( _vendor === '' ) return style;
    return _vendor + style.charAt(0).toUpperCase() + style.substr(1);
  }

  function _translate(x, y, node_style) {
    (node_style || scroll_node_style)[_transform] = 'translate3d('+x+'px,'+y+'px, 0)';
  }

  return {
    setNode: function(node_x, node_y)
    {
      scroll_nodes.x = node_x;
      scroll_nodes.y = node_y;
    },
    setCellSize: function(value)
    {
      cell_size = value;
    },
    setCallbackOnMove: function(callback)
    {
      move_cb = callback;
    },
    setCallbackOnMoveEnd: function(callback)
    {
      move_end_cb = callback;
    },
    _start: function(e)
    {
      var point = e.touches ? e.touches[0] : e;
      start_x = last_x = point.pageX;
      start_y = last_y = point.pageY;
      move_offset = 0;
      move_axis = false;
      _translate(0, 0, scroll_nodes.x.style);
      _translate(0, 0, scroll_nodes.y.style);
    },

    _move: function(e)
    {
      e.preventDefault();
      var point = e.touches ? e.touches[0] : e;

      if (!move_axis) {
        move_axis = Math.abs(point.pageX-start_x) > Math.abs(point.pageY-start_y) ? 'x' : 'y' ;
        scroll_node_style = scroll_nodes[move_axis].style;
        move_cb(move_axis);
      }

      if (move_axis === 'x') {
        _translate(point.pageX-start_x, 0);
        move_offset = point.pageX-start_x;
        move_direction = point.pageX > last_x ? 1 : -1 ;
        last_x = point.pageX;
      }
      else {
        _translate(0, point.pageY-start_y);
        move_offset = point.pageY-start_y;
        move_direction = point.pageY > last_y ? 1 : -1 ;
        last_y = point.pageY;
      }
    },

    _end: function(e) 
    {
      if (!move_axis) {
        return;
      }

      var cell_offset = 0,
          final_offset = 0,
          timer,
          timer_counter = 1,
          step_timer = 10,
          step_counter = 0.25,
          offset_delta = 0;

      cell_offset = move_direction > 0 ? 
        Math.ceil(move_offset / cell_size) : Math.floor(move_offset / cell_size) ;
      // cell_offset =  Math[move_direction>0?'ceil':'floor'](move_offset / cell_size);
      final_offset = cell_offset * cell_size;
      offset_delta = (move_offset - final_offset);

      timer = setInterval(function(){
        timer_counter -= step_counter;

        if (move_axis === 'x') {
          _translate(final_offset+offset_delta*timer_counter, 0);
        }
        else {
          _translate(0, final_offset+offset_delta*timer_counter);
        }

        if (timer_counter <= 0) {
          clearTimeout(timer);
          move_end_cb(cell_offset, move_axis);
        }
      }, step_timer)

    }
  };
});
