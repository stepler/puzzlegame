/*global define*/
define([
  'zepto',
  'lodash'
], function ($, _) {
  'use strict';

  var $html = $('html'),
      default_options = {},
      admobid = {},
      is_enable = !!window.AdMob,
      refresh_timer = null;

  if (!is_enable) {
    return function(){};
  }

  if (/(android)/i.test(navigator.userAgent) ) {
    admobid = { // for Android
      banner: 'ca-app-pub-4830732904362065/3682751233'
    };
  } else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
    admobid = { // for iOS
      banner: 'ca-app-pub-4830732904362065/8112950833'
    };
  }

  var default_options = {
      adSize: 'SMART_BANNER',
      position: AdMob.AD_POSITION.TOP_CENTER,
      color_bg: '000000',
      isTesting: true,
      autoShow: true
  };
  AdMob.setOptions(default_options);

  function createBanner()
  {
    AdMob.removeBanner();
    AdMob.createBanner(admobid.banner);
  }

  $(document).on('onAdDismiss', function() {
    refresh_timer = setTimeout(createBanner, 30*1000);
  });

  $(document).on('onAdFailLoad', function() { createBanner(); });

  return createBanner;
});
