/*global define*/
define([
  'zepto',
  'backbone',
  'core/navigator',
  'views/main',
  'views/level',
  'views/game',
  'views/game_complete',
  'views/game_pause',
  'views/game_end',
  'views/error500',
  'common'
], function ($, Backbone, Navigator, MainView, LevelView,
  GameView, GameCompleteView, GamePauseView, GameEndView, Error500View, Common) {

  'use strict';

  var Router = Backbone.Router.extend({
    routes: {
      '': 'mainPage',
      'level/:level_type': 'levelPage',
      'game/end': 'gameEndPage',
      'game/:game_id': 'gamePage',
      'game/:game_id/pause': 'gamePausePage',
      'game/:game_id/error': 'gameErrorPage',
      'game/:game_id/complete': 'gameCompletePage',
      'error500': 'error500Page'
    },

    mainPage: function() {
      Navigator.openPage(MainView);
    },
    levelPage: function(level_type) {
      LevelView.setLevel(level_type);
      Navigator.openPage(LevelView);
    },
    gamePage: function(game_id) {
      GameView.setGameId(game_id);
      Navigator.openPage(GameView);
    },
    gamePausePage: function(game_id) {
      GamePauseView.setGameId(game_id);
      GamePauseView.setGameView(GameView);
      Navigator.popupPage(GamePauseView);
    },
    gameCompletePage: function(game_id) {
      GameCompleteView.setGameId(game_id);
      GameCompleteView.setGameStatus(true);
      Navigator.openPage(GameCompleteView);
    },
    gameErrorPage: function(game_id) {
      GameCompleteView.setGameId(game_id);
      GameCompleteView.setGameStatus(false);
      Navigator.openPage(GameCompleteView);
    },
    gameEndPage: function() {
      Navigator.openPage(GameEndView);
    },
    error500Page: function()
    {
      Navigator.openPage(Error500View)
    }
  });

  return Router;
});
