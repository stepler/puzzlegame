/*global define*/
define([
  'zepto',
  'lodash',
  'common'
], function ($, _, Backbone, Common) {
  'use strict';
  var SCREEN_WIDTH = 320,
      SCREEN_HEIGHT = 480;

  var active_view = {
    $el:$('#not-exist-id'),
    alias:'main',
    freeze: function(){},
    destroy: function(){}
  };

  function getDirection(next_alias, active_alias)
  {
    var dw = {
      'main': 0,
      'level': 1,
      'game': 2,
      'game_complete': 3,
      'game_pause': 4,
    };
    return (dw[next_alias] || 0) >= (dw[active_alias] || 0) ? 'next' : 'prev';
  }

  return {
    openPage: function(view, without_destroy)
    {
      var direction = getDirection(view.alias, active_view.alias);
      active_view.freeze();
      view.$el.attr('class', 'b-page open-'+direction);
      view.render();
      active_view.$el.attr('class', 'b-page close-'+direction);
      setTimeout(function()
      {
        view.$el.attr('class', 'b-page open open-'+direction);
        active_view.$el.attr('class', 'b-page close close-'+direction);
        setTimeout(function()
        {
          view.$el.attr('class', 'b-page open');
          active_view.$el.attr('class', 'b-page');
          if (!without_destroy) {
            active_view.destroy();
          }
          active_view = view;
        }, 500)
      }, 100);
    },
    popupPage: function(view)
    {
      this.openPage(view, true);
    }
  };
});
