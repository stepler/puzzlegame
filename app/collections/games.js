/*global define */
define([
  'lodash',
  'backbone',
  'backboneLocalstorage',
  'models/game'
], function (_, Backbone, Store, Game) {
  'use strict';

  var Games = Backbone.Collection.extend({

    model: Game,
    comparator: 'sort',

    localStorage: new Store('game.games'),

    getLevelGames: function(level)
    {
      return _.map(this.where({level: level}), function(model){ return model.toJSON(); });
    },

    getNextGame: function(game)
    {
      var next_game = game.get('next_game');
      return this.findWhere({level: next_game[0], alias: next_game[1]});
    },

    nextGameIsEnd: function(game)
    {
      var next_game = game.get('next_game');
      return next_game[0] === 'all_complete';
    },

    allGamesIsComplete: function(level)
    {
      var list = this.where({level: level, is_complete: false});
      return list.length === 0;
    }
  });

  return new Games();
});
