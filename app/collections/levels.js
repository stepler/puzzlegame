/*global define */
define([
  'lodash',
  'backbone',
  'backboneLocalstorage',
  'models/level'
], function (_, Backbone, Store, Level) {
  'use strict';

  var Levels = Backbone.Collection.extend({

    model: Level,

    localStorage: new Store('game.levels'),

    getLevel: function(type)
    {
      return this.findWhere({type: type});
    },

    openNextLevels: function(level)
    {
      var i,
          next_level,
          next_levels = level.get('next_levels');

      for (i in next_levels) {
        next_level = this.getLevel(next_levels[i]);
        if (next_level) {
          next_level.openLevel();
        }
      }
    }
  });

  return new Levels();
});
