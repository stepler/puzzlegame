/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/games',
  'collections/settings',
  'text!templates/game-complete.html',
  'common'
], function ($, _, Backbone, GamesCollection, Settings, gameCompleteTemplate, Common) {
  'use strict';

  var GameComplete = Backbone.View.extend({

    alias: 'game_complete',
    el: '#game_complete',
    game_id: null,
    game_status: null,

    template: _.template(gameCompleteTemplate),

    events: {},

    initialize: function(level)
    {
    },

    setGameId: function(value)
    {
      this.game_id = value;
    },

    setGameStatus: function(value)
    {
      this.game_status = value;
    },

    render: function(level)
    {
      var next_game,
          next_is_main = false,
          game = GamesCollection.get(this.game_id);

      if (!game) {
        console.error('Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      if (game.get('level') == 'x3' && (game.get('alias') == '1')) {
        Common.isTutorialPass = true;
        Settings.setKey('isTutorialPass', '1');
      }

      next_game = game;
      if (this.game_status) {
        next_game = GamesCollection.getNextGame(game);
      }

      if (!next_game) {
        if (GamesCollection.nextGameIsEnd(game)) {
          Common.router.navigate('#/game/end');
          return;
        }
        console.error('Next Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      if (game.get('level') != next_game.get('level')) {
        next_is_main = true;
      }

      this.$el.html(this.template({
        is_complete: this.game_status,
        next_game_id: next_game.get('id'),
        level_id: game.get('level'),
        next_is_main: next_is_main,
        description: ''
      }));
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new GameComplete();
});
