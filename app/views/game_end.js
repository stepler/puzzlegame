/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'text!templates/game-end.html',
  'common'
], function ($, _, Backbone, gameEndTemplate, Common) {
  'use strict';

  var GameEndView = Backbone.View.extend({

    alias: 'game_end',
    el: '#game_end',

    template: _.template(gameEndTemplate),

    events: {},

    initialize: function(level)
    {
    },

    render: function(level)
    {
      this.$el.html(this.template());
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new GameEndView();
});
