/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/levels',
  'text!templates/main.html',
  'common'
], function ($, _, Backbone, LevelsCollection, mainTemplate, Common) {
  'use strict';

  var MainView = Backbone.View.extend({

    alias: 'main',
    el: '#main',

    template: _.template(mainTemplate),

    events: {

    },

    initialize: function ()
    {
      // this.render();
    },

    render: function()
    {
      this.$el.html(this.template({
        levels: LevelsCollection.toJSON()
      }));
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new MainView();
});
