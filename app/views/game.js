/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/levels',
  'collections/games',
  'text!templates/game.html',
  'core/puzzle_scroll',
  'common'
], function ($, _, Backbone, LevelsCollection, GamesCollection, gameTemplate, pScroll, Common) {
  'use strict';

  var ratingTimer, startupTimer, 
      isTutorial, tutorialGame, tutorialStep=0;

  var GameView = Backbone.View.extend({

    alias: 'game',
    el: '#game',
    game: null,
    game_id: null,
    level: null,
    size: 0,
    rating_value: 0,
    game_award: '',


    template: _.template(gameTemplate),

    events: {},

    initialize: function()
    {
    },

    setGameId: function(value)
    {
      this.game_id = value;
    },

    render: function()
    {
      var game, level, field_size;


      if (this.rating_value !== 0) {
        return this.resume_game();
      }

      game = GamesCollection.get(this.game_id);
      if (!game) {
        console.error('Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      level = LevelsCollection.getLevel(game.get('level'));
      if (!level) {
        console.error('Game level not found');
        Common.router.navigate('#/error500');
        return;
      }

      field_size = this.calcPuzzleSize(game.get('size'));
      if (!field_size) {
        console.error('Invalid game size');
        Common.router.navigate('#/error500');
        return;
      }

      isTutorial = false;
      if (game.get('level') == 'x3' && (game.get('alias') == '1')) {
        isTutorial = !Common.isTutorialPass;
        tutorialGame = game.get('alias');
      }

      this.size = parseInt(game.get('size'));
      this.game = game;
      this.level = level;

      this.$el.html(this.template({
        game_id: game.get('id'),
        level_title: level.get('title'),
        game_title: game.get('title'),
        puzzle: game.get('puzzle'),
        puzzle_shuffle: this.genShuffleGame(game.get('puzzle')),
        field_size: field_size,
        scroll_size: _.range(0, this.size*3)
      }));

      this.launchGame(field_size.cell);
    },

    pause_game: function()
    {
      this.time_passed = _.now() - this.time_start;
    },

    resume_game: function() {},

    launchGame: function(cell_size)
    {
      var self = this;
      var tmp,
          touch_width,
          touch_height,
          touch_top,
          touch_left,
          active_axis,
          active_row,
          active_col,
          rigth_puzzle,
          user_puzzle;

      var $touch,
          $puzzle_startup,
          $puzzle_status,
          $puzzle_canvas,
          $puzzle_pad,
          $puzzle_tutorial,
          $puzzle_toggle_canvas,
          $scroll_x, $scroll_y,
          $scroll_x_items, $scroll_y_items,
          $pad_items, pad_items,
          $puzzle_award_gold, $puzzle_award_silver, $puzzle_award_bronze;

      var pad_item_fill_class = 'cell-f',
          scroll_item_class_empty = 'b-puzzle__cell cell-e',
          scroll_item_class_fill = 'b-puzzle__cell cell-f';

      $touch = $('#puzzle-touch', this.el);
      $scroll_x = $('#puzzle-scroll-x', this.el);
      $scroll_y = $('#puzzle-scroll-y', this.el);
      $scroll_x_items = $scroll_x.children();
      $scroll_y_items = $scroll_y.children();

      $puzzle_pad = $('#puzzle-pad', this.el);
      $puzzle_canvas = $('#puzzle-canvas', this.el);
      $puzzle_toggle_canvas = $('#puzzle-toggle-canvas', this.el);
      $puzzle_status = $('#puzzle-status', this.el);
      $puzzle_startup = $('#puzzle-startup', this.el);
      $puzzle_tutorial = $('#puzzle-tutorial', this.el);

      $puzzle_award_gold = $('#puzzle-award-gold');
      $puzzle_award_silver = $('#puzzle-award-silver');
      $puzzle_award_bronze = $('#puzzle-award-bronze');

      $pad_items = $('#puzzle-pad', this.el).children();

      pScroll.setNode($scroll_x.get(0), $scroll_y.get(0));
      pScroll.setCellSize(cell_size);
      pScroll.setCallbackOnMove(showMovedScroll);
      pScroll.setCallbackOnMoveEnd(disableScroll);

      $touch.on('touchstart', enabeScroll);

      $touch.on('touchstart', pScroll._start);
      $touch.on('touchmove', pScroll._move);
      $touch.on('touchend', pScroll._end);

      tmp = $touch.offset();
      touch_width = tmp.width;
      touch_height = tmp.height;
      touch_top = tmp.top;
      touch_left = $('body').offset().left;

      rigth_puzzle = this.game.get('puzzle');
      user_puzzle = [];

      toggleCanvas();
      preparePadItems();

      // >>> Tutorial -- Prepare Tutorial pin
        if (isTutorial) {
        this.tutorialLaunchGame($puzzle_tutorial, $touch);
      }
      // <<< Tutorial

      function preparePadItems()
      {
        var is_equal = true;
        $pad_items.each(function(key) {
          this.is_fill = $(this).hasClass(pad_item_fill_class);
          user_puzzle[key] = this.is_fill ? 1 : 0;
        });
        is_equal = _.isEqual(rigth_puzzle, user_puzzle);

        if (is_equal) {
          return completeGame();
        }

        // >>> Tutorial -- Disable rating
        if (isTutorial) {
          return self.tutorialPreparePadItems($puzzle_tutorial, $pad_items, 
            user_puzzle, [scroll_item_class_empty, scroll_item_class_fill]);
        }
        // <<< Tutorial
      }

      function enabeScroll(e)
      {
        var cell,
            point = e.touches ? e.touches[0] : e;

        cell = getTouchCell();

        active_row = getFillFromPad(cell.coords[1], 'x');
        active_col = getFillFromPad(cell.coords[0], 'y');

        fillScroll($scroll_x_items, active_row, $scroll_x.get(0), 'x', cell.offset[1]);
        fillScroll($scroll_y_items, active_col, $scroll_y.get(0), 'y', cell.offset[0]);

        function getTouchCell()
        {
          var cell_size = touch_width / self.size,
              cell_x = Math.floor((point.pageX-touch_left)/cell_size),
              cell_y = Math.floor((point.pageY-touch_top)/cell_size);
          return {coords: [cell_x, cell_y], offset: [cell_x*cell_size, cell_y*cell_size]};
        }

        function getFillFromPad(cell, axis)
        {
          var fill = [];
          if (axis === 'x') {
            $pad_items.each(function(i){
              if (Math.floor(i / self.size) === cell) {
                fill.push(this);
              }
            });
          }
          else {
            $pad_items.each(function(i){
              if (i%self.size === cell) {
                fill.push(this);
              }
            });
          }
          return fill;
        }

        function fillScroll($list, data, scroll_node, axis, position)
        {
          var i;
          $list.each(function(i) {
            this.is_fill = data[i%self.size].is_fill;
            this.className = this.is_fill ? scroll_item_class_fill : scroll_item_class_empty ;
          });
          scroll_node.style[axis==='x'?'top':'left'] = position+'px';
        }
      }

      function showMovedScroll(axis)
      {
        var i,
            data = axis === 'x' ? active_row : active_col ,
            scroll_node = axis === 'x' ? $scroll_x.get(0) : $scroll_y.get(0) ;

        scroll_node.style.visibility = 'visible';
        for (i in data) {
          data[i].style.visibility = 'hidden';
        }
      }

      function disableScroll(offset, axis)
      {
        offset *= -1;
        var i, item,
            data = axis === 'x' ? active_row : active_col ,
            scroll_node = axis === 'x' ? $scroll_x.get(0) : $scroll_y.get(0) ,
            $scroll_items = axis === 'x' ? $scroll_x_items : $scroll_y_items ;

        for (i in data) {
          item = data[i];
          item.is_fill = $scroll_items.get(parseInt(i)+offset).is_fill;
          item.className = item.is_fill ? scroll_item_class_fill : scroll_item_class_empty ;
        }

        scroll_node.style.visibility = 'hidden';
        for (i in data) {
          data[i].style.visibility = 'visible';
        }

        preparePadItems();
      }

      function startRatingTimeout()
      {
        // >>> Tutorial -- Disable rating
        if (isTutorial) {
          self.game_award = 'g';
          return;
        }
        // <<< Tutorial

        var step = 0.25,
            interval = 0,
            awards_timer = self.game.get('awards_timer'),
            awards_list = ['g', 's', 'b'],
            $nodes = [
              $puzzle_award_gold.get(0),
              $puzzle_award_silver.get(0),
              $puzzle_award_bronze.get(0)
            ];

        if (!awards_timer) {
          return;
        }

        interval = awards_timer / (10 / step);
        ratingTimer = setInterval(function()
        {
          var award = Math.floor(self.rating_value / 10),
              value = 1 - ((self.rating_value % 10) * 0.1);
          self.rating_value += step;

          self.game_award = awards_list[award];
          if (award > 2) {
            clearInterval(ratingTimer);
            self.game_award = null;
          }

          if (value === 1) {
            award -= 1;
            value = 0;
          }

          if ($nodes[award]) {
            $nodes[award].style.opacity = value;
          }
        }, interval);
      }

      function toggleCanvas()
      {
        $puzzle_toggle_canvas.on('touchstart', function(){
          $puzzle_canvas.css('opacity', '1');
        });
        $puzzle_toggle_canvas.on('touchend', function(){
          $puzzle_canvas.css('opacity', '0');
        });
      }

      function completeGame()
      {
        clearInterval(ratingTimer);
        self.completeAndOpenNext();
        setTimeout(function() {
          $puzzle_pad.addClass('highlight');
          setTimeout(function() {
            Common.router.navigate('#/game/'+self.game.get('id')+'/complete');
          }, 500);
        }, 250);
      }

      function start_game()
      {
        var timer = 3;
        $puzzle_canvas.css('opacity', '1');
        $puzzle_status.css('opacity', '0');
        $puzzle_startup.css('opacity', '1');

        $puzzle_startup.text(timer);
        startupTimer = setInterval(function(){
          timer -= 1;
          if (timer > 0) {
            return $puzzle_startup.text(timer);
          }
          $puzzle_canvas.css('opacity', '0');
          $puzzle_status.css('opacity', '1');
          $puzzle_startup.css('opacity', '0');

          clearInterval(startupTimer);
          setTimeout(startRatingTimeout, 500);
        }, 1000);
      }

      this.resume_game = start_game;

      start_game();
    },

    completeAndOpenNext: function()
    {
      if (this.game_award) {
        this.game.setAward(this.game_award);
      }

      var next_game = GamesCollection.getNextGame(this.game);

      this.game.completeGame();

      if (next_game) {
        next_game.openGame();
      }

      if (GamesCollection.allGamesIsComplete(this.game.get('level'))) {
        this.level.completeLevel();
        LevelsCollection.openNextLevels(this.level);
      }
    },

    calcPuzzleSize: function(game_size)
    {
      // game_size = parseInt(game_size);
      var field_size, cell_size, offset_field_size,
          cell_margin = 1,
          screen_width = window.WIDTH || 0;
      if (screen_width === 0) {
        return null;
      }

      cell_size = Math.floor(screen_width / game_size);
      field_size = cell_size * game_size;
      cell_size -= cell_margin * 2;
      offset_field_size = (screen_width - field_size) / 2;

      return {
        offset: offset_field_size,
        field: field_size,
        cell: cell_size
      }
    },

    genShuffleGame: function(puzzle)
    {
      var shuffle = Array.prototype.slice.call(puzzle);
      while (_.isEqual(shuffle, puzzle)) {
        shuffle = _.shuffle(shuffle);
      }
      return shuffle;
    },

    freeze: function()
    {
      clearInterval(ratingTimer);
      clearInterval(startupTimer);
      this.pause_game();
    },

    destroy: function()
    {
      clearInterval(ratingTimer);
      clearInterval(startupTimer);
      this.rating_value = 0;
      this.$el.empty()

      tutorialStep = 0;
    },

    // Tutorial
    tutorialLaunchGame: function($puzzle_tutorial, $touch)
    {
      $puzzle_tutorial.css('display', 'block');
      $touch.on('touchstart', function(){ $puzzle_tutorial.css('display', 'none'); });
    },

    tutorialPreparePadItems: function($puzzle_tutorial, $pad_items, user_puzzle, classNames)
    {
      var pad_items, prev_pad_items,
          tutorial_def_class = 'b-tutorial__pin game-';
      tutorialStep += 1;

      $puzzle_tutorial.css('display', 'block');

      if (tutorialGame === '1') {
        pad_items = [0,1,0, 0,0,0, 0,0,0];
        if (tutorialStep == 1) {
          setTimeout(function() {
            $puzzle_tutorial.attr('class', tutorial_def_class+'1_1');
          }, 3000);
        }
      }

      // if (tutorialGame === '2') {
      //   if (tutorialStep == 1) {
      //     pad_items = [0,0,1, 1,0,1, 0,0,0];
      //   }
      //   if (tutorialStep == 2) {
      //     pad_items = [1,0,0, 1,0,1, 0,0,0];
      //     if (!_.isEqual(pad_items, user_puzzle)) {
      //       pad_items = [0,0,1, 1,0,1, 0,0,0];
      //       tutorialStep = 1;
      //     }
      //   }
      //   if (tutorialStep == 3) {
      //     pad_items = [1,0,0, 0,1,1, 0,0,0];
      //     if (!_.isEqual(pad_items, user_puzzle)) {
      //       pad_items = [1,0,0, 1,0,1, 0,0,0];
      //       tutorialStep = 2;
      //     }
      //   }
      //   if (tutorialStep == 4) {
      //     pad_items = [1,0,0, 0,1,1, 0,0,0];
      //     tutorialStep = 3;
      //   }
      //   $puzzle_tutorial.attr('class', tutorial_def_class+'2_'+tutorialStep);
      // }

      $pad_items.each(function(key){
        this.is_fill = !!pad_items[key];
        this.className = pad_items[key] ? classNames[1] : classNames[0] ;
      });

    }
  });

  return new GameView();
});
