/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/games',
  'text!templates/game-pause.html',
  'common'
], function ($, _, Backbone, GamesCollection, gamePauseTemplate, Common) {
  'use strict';

  var GamePause = Backbone.View.extend({

    alias: 'game_pause',
    el: '#game_pause',
    game_id: null,
    game_view: null,

    template: _.template(gamePauseTemplate),

    events: {},

    initialize: function() {},

    setGameId: function(value)
    {
      this.game_id = value;
    },

    setGameView: function(value)
    {
      this.game_view = value;
    },

    render: function(level)
    {
      var game, self = this;

      game = GamesCollection.get(this.game_id);
      if (!game) {
        console.error('Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      this.$el.html(this.template({
        level_id: game.get('level'),
        game_id: this.game_id,
        description: ''
      }));

      $('#pause-exit-btn', this.el).on('click', function() {
        self.game_view && self.game_view.destroy();
      });
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new GamePause();
});
