/*global define*/
define([
  'lodash',
  'backbone'
], function (_, Backbone) {
  'use strict';

  var Game = Backbone.Model.extend({

    defaults: {
      level: '',
      sort: 0,
      alias: '',
      title: '',
      score: 0,
      timer: 0,
      size: 0,
      puzzle: 0,
      award: null,
      awards_timer: 0,
      next_game: [],
      is_open: false,
      is_complete: false
    },

    openGame: function()
    {
      this.save({
        is_open: true
      });
    },

    completeGame: function() {
      this.save({
        is_complete: true
      });
    },

    setAward: function(award) {
      var weight = {g:3, s:2, b:1},
          current_award = this.get('award');
      if (['g','s','b'].indexOf(award) === -1) {
        return;
      }
      if (weight[current_award] >= weight[award]) {
        return;
      }
      this.save({
        award: award
      });
    }
  });

  return Game;
});
