/*global define*/
define([
  'lodash',
  'backbone'
], function (_, Backbone) {
  'use strict';

  var Level = Backbone.Model.extend({

    defaults: {
      title: '',
      type: '',
      next_levels: [],
      is_open: false,
      is_complete: false
    },

    openLevel: function()
    {
      this.save({
        is_open: true
      });
    },

    completeLevel: function() 
    {
      this.save({
        is_complete: true
      });
    }
  });

  return Level;
});
